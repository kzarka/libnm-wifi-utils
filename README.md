# libnm-wifi-utils

A collection of basic utility functions implemented in C++ using libnm for managing Wi-Fi connections with NetworkManager.

## Getting started

To get started with GitLab, follow these steps:

Clone the repository.
Build the desired functionality using g++ -o function-name.cpp `pkg-config --cflags --libs libnm`.
Run the desired utility functions.

## Usage
The libnm-wifi-utils library provides the following functions:
enable_wifi.cpp: enables Wi-Fi network using NetworkManager.
get_signal_level.cpp: returns the signal level.
disconnect_wifi.cpp: disconnects from the active Wi-Fi connection using NetworkManager.
disable_wifi.cpp: disables the Wi-Fi using NetworkManager.
is_wifi_active.cpp: checks if the wifi is enabled using NetworkManager.
