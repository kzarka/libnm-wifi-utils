#include <iostream>
#include <glib.h>
#include <NetworkManager.h>

const int ENABLE_WIFI_MAX_ATTEMPTS = 10;

struct WifiStatus
{
    bool wifi_enabled;
    NMClient *client;
    GMainLoop *loop;
    int attempts;
};

static gboolean check_wifi_status(gpointer user_data)
{
    WifiStatus *wifi_status = (WifiStatus *)user_data;
    wifi_status->wifi_enabled = nm_client_wireless_get_enabled(wifi_status->client);
    wifi_status->attempts++;
    if (wifi_status->wifi_enabled || wifi_status->attempts >= ENABLE_WIFI_MAX_ATTEMPTS)
    {
        g_main_loop_quit(wifi_status->loop);
    }
    return !wifi_status->wifi_enabled && wifi_status->attempts < ENABLE_WIFI_MAX_ATTEMPTS;
}

bool enable_wifi_with_attempts()
{
    GError *error = NULL;
    NMClient *client;
    GMainLoop *loop = g_main_loop_new(NULL, FALSE);
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        g_print("Could not connect to NetworkManager: %s\n", error->message);
        g_error_free(error);
        return false;
    }
    WifiStatus wifi_status = {false, client, loop, 0};
    nm_client_wireless_set_enabled(client, TRUE);
    g_timeout_add_seconds(1, check_wifi_status, &wifi_status);
    g_main_loop_run(loop);
    bool result = wifi_status.wifi_enabled;
    g_object_unref(client);
    g_main_loop_unref(loop);
    return result;
}

int main()
{
    bool result = enable_wifi_with_attempts();
    if (result)
    {
        g_print("Wi-Fi got enabled\n");
    }
    else
    {
        g_print("Wi-Fi is not enabled\n");
    }
    return 0;
}