#include <NetworkManager.h>
#include <glib.h>
#include <iostream>

bool isWifiActive()
{
    NMClient *client = NULL;
    GError *error = NULL;
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        std::cout << "Error: " << error->message << std::endl;
        g_error_free(error);
        return false;
    }
    if (nm_client_wireless_get_enabled(client))
    {
        std::cout << "Wi-Fi is active." << std::endl;
        g_object_unref(client);
        return true;
    }
    else
    {
        std::cout << "Wi-Fi is not not active." << std::endl;
        g_object_unref(client);
        return false;
    }
}
int main()
{
    if (isWifiActive())
    {
        std::cout << "Wi-Fi is active." << std::endl;
    }
    else
    {
        std::cout << "Wi-Fi is not active." << std::endl;
    }

    return 0;
}
