#include <iostream>
#include <NetworkManager.h>
#include <vector>
#include <glib.h>

void addAndActivateConnectionCallback(GObject *object, GAsyncResult *res, gpointer user_data)
{
    GError *error = nullptr;
    NMClient *client = NM_CLIENT(object);
    GMainLoop *mainLoop = static_cast<GMainLoop *>(user_data);

    nm_client_add_and_activate_connection_finish(client, res, &error);
    if (error)
    {
        std::cerr << "Error adding and activating connection: " << error->message << std::endl;
        g_error_free(error);
    }
    else
    {
        std::cout << "Connection added and activated successfully!" << std::endl;
    }

    g_main_loop_quit(mainLoop);
}

int main()
{
    NMClient *client = nm_client_new(NULL, NULL);

    // Retrieve the Wi-Fi device
    NMDevice *wifiDevice = NULL;
    const GPtrArray *devices = nm_client_get_devices(client);
    if (devices != NULL)
    {
        for (guint i = 0; i < devices->len; i++)
        {
            NMDevice *device = NM_DEVICE(devices->pdata[i]);
            if (nm_device_get_device_type(device) == NM_DEVICE_TYPE_WIFI)
            {
                wifiDevice = device;
                g_object_ref(wifiDevice); // Increment reference count
                break;
            }
        }
        g_ptr_array_unref(const_cast<GPtrArray *>(devices));
    }

    if (wifiDevice == NULL)
    {
        std::cerr << "No Wi-Fi device found." << std::endl;
        g_object_unref(client);
        return 1;
    }

    // Create a new connection profile
    NMConnection *connection = nm_simple_connection_new();

    // Create and set the connection settings
    NMSettingConnection *connectionSetting = NM_SETTING_CONNECTION(nm_setting_connection_new());
    g_object_set(G_OBJECT(connectionSetting),
                 NM_SETTING_CONNECTION_ID, "Zarka",
                 NM_SETTING_CONNECTION_UUID, nm_utils_uuid_generate(),
                 NM_SETTING_CONNECTION_TYPE, NM_SETTING_WIRELESS_SETTING_NAME,
                 NM_SETTING_CONNECTION_AUTOCONNECT, FALSE,
                 NULL);
    nm_connection_add_setting(connection, NM_SETTING(connectionSetting));

    // Create and set the wireless settings
    NMSettingWireless *wirelessSetting = NM_SETTING_WIRELESS(nm_setting_wireless_new());
    int ssidSize = 5; // Length of "Zarka"
    GBytes *ssid = g_bytes_new("Zarka", ssidSize);
    g_object_set(G_OBJECT(wirelessSetting),
                 NM_SETTING_WIRELESS_SSID, ssid,
                 NM_SETTING_WIRELESS_MODE, NM_SETTING_WIRELESS_MODE_INFRA,
                 NULL);
    nm_connection_add_setting(connection, NM_SETTING(wirelessSetting));

    // Create and set the wireless security settings
    NMSettingWirelessSecurity *wirelessSecuritySetting = NM_SETTING_WIRELESS_SECURITY(nm_setting_wireless_security_new());
    g_object_set(G_OBJECT(wirelessSecuritySetting),
                 NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "wpa-psk",
                 NM_SETTING_WIRELESS_SECURITY_PSK, "the_zarka@123",
                 NULL);
    nm_connection_add_setting(connection, NM_SETTING(wirelessSecuritySetting));

    GMainLoop *mainLoop = g_main_loop_new(NULL, FALSE);

    // Increment reference counts for connection and client objects
    // to prevent premature destruction during asynchronous operation
    g_object_ref(connection);
    g_object_ref(client);

    nm_client_add_and_activate_connection_async(client, connection, wifiDevice, nullptr, nullptr, addAndActivateConnectionCallback, mainLoop);

    // Start the main loop to process asynchronous operations
    g_main_loop_run(mainLoop);

    g_main_loop_unref(mainLoop);

    g_object_unref(connection);
    g_object_unref(client);

    return 0;
}
