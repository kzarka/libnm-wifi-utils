#include <iostream>
#include <glib.h>
#include <NetworkManager.h>
#include <vector>

const int MAX_ATTEMPTS = 10;

enum BandType
{
    BAND_NONE = 0, /* unknown */
    BAND_2GHZ = 1, /* 2.4GHz */
    BAND_5GHZ = 2, /* 5GHz */
    BAND_ANY = 3   /* Dual-mode frequency band */
};

enum WifiSecurityType
{
    WIFI_SEC_TYPE_INVALID = 0,
    WIFI_SEC_TYPE_OPEN = 1,
    WIFI_SEC_TYPE_WEP = 2,
    WIFI_SEC_TYPE_PSK = 3,
    WIFI_SEC_TYPE_SAE = 4,
};

struct AccessPointInfo
{
    std::string ssid;
    std::string bssid;
    int rssi;
    int frequency;
    BandType band;
    WifiSecurityType security;
};
struct WifiStatus
{
    bool wifi_enabled;
    NMClient *client;
    GMainLoop *loop;
    NMDevice *wifi_device;
    int attempts;
};

BandType get_band(guint32 frequency)
{
    if (frequency >= 2400 && frequency <= 2495)
    {
        return BAND_2GHZ;
    }
    else if (frequency >= 5150 && frequency <= 5925)
    {
        return BAND_5GHZ;
    }
    else
    {
        return BAND_NONE;
    }
}
WifiSecurityType get_wifi_security_type(NMAccessPoint *ap)
{
    NM80211ApSecurityFlags wpa_flags = nm_access_point_get_wpa_flags(ap);
    NM80211ApSecurityFlags rsn_flags = nm_access_point_get_rsn_flags(ap);

    if (wpa_flags & NM_802_11_AP_SEC_KEY_MGMT_PSK || rsn_flags & NM_802_11_AP_SEC_KEY_MGMT_PSK)
    {
        return WIFI_SEC_TYPE_PSK;
    }
    else if (wpa_flags & NM_802_11_AP_SEC_KEY_MGMT_802_1X || rsn_flags & NM_802_11_AP_SEC_KEY_MGMT_802_1X)
    {
        return WIFI_SEC_TYPE_SAE;
    }
    else if (wpa_flags == 0 && rsn_flags == 0)
    {
        return WIFI_SEC_TYPE_OPEN;
    }
    else
    {
        return WIFI_SEC_TYPE_INVALID;
    }
}
void on_wifi_scan_finished(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
    WifiStatus *wifi_status = (WifiStatus *)user_data;
    GError *error = NULL;
    gboolean success = nm_device_wifi_request_scan_finish(NM_DEVICE_WIFI(source_object), res, &error);
    if (!success)
    {
        if (g_strrstr(error->message, "Scanning not allowed") != NULL)
        {
            // Scanning not allowed immediately following previous scan
            // Retry after a short delay
            g_print("Scanning not allowed, retrying in 4 seconds...\n");
            g_error_free(error);
            sleep(4);
            nm_device_wifi_request_scan_async(NM_DEVICE_WIFI(source_object), NULL, on_wifi_scan_finished, wifi_status);
            return;
        }

        g_print("Error scanning for WiFi networks: %s\n", error->message);
        g_error_free(error);
    }
    GMainLoop *loop = wifi_status->loop;
    g_main_loop_quit(loop);
}

bool scan_wifi(WifiStatus *wifi_status)
{
    GMainLoop *loop = g_main_loop_new(NULL, FALSE);
    wifi_status->loop = loop;
    nm_device_wifi_request_scan_async(NM_DEVICE_WIFI(wifi_status->wifi_device), NULL, on_wifi_scan_finished, wifi_status);
    g_main_loop_run(loop);
    g_main_loop_unref(loop);
    return true;
}
std::vector<AccessPointInfo> get_scan_info(WifiStatus *wifi_status)
{
    std::vector<AccessPointInfo> ap_info_list;

    if (wifi_status->wifi_device != NULL)
    {
        const GPtrArray *aps = nm_device_wifi_get_access_points(NM_DEVICE_WIFI(wifi_status->wifi_device));
        if (aps != NULL)
        {
            for (guint i = 0; i < aps->len; i++)
            {
                NMAccessPoint *ap = (NMAccessPoint *)g_ptr_array_index(aps, i);
                GBytes *ssid_bytes = nm_access_point_get_ssid(ap);
                if (ssid_bytes != NULL)
                {
                    AccessPointInfo ap_info;
                    gsize ssid_len;
                    const gchar *ssid_str = (const gchar *)g_bytes_get_data(ssid_bytes, &ssid_len);
                    ap_info.ssid.assign(ssid_str, ssid_len);
                    g_bytes_unref(ssid_bytes);
                    ap_info.bssid = nm_access_point_get_bssid(ap);
                    ap_info.rssi = nm_access_point_get_strength(ap);
                    ap_info.frequency = nm_access_point_get_frequency(ap);
                    ap_info.band = get_band(nm_access_point_get_frequency(ap));
                    ap_info.security = get_wifi_security_type(ap);
                    ap_info_list.push_back(ap_info);
                }
            }
        }
    }

    return ap_info_list;
}
bool init_wifi_device(WifiStatus *wifi_status)
{
    GError *error = NULL;
    NMClient *client = NULL;
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        g_print("Could not connect to NetworkManager: %s\n", error->message);
        g_error_free(error);
        return false;
    }
    wifi_status->client = client;

    const GPtrArray *devices = nm_client_get_devices(client);
    if (devices == NULL)
    {
        g_print("Could not get devices: %s\n", error->message);
        g_error_free(error);
        return false;
    }

    for (guint i = 0; i < devices->len; i++)
    {
        NMDevice *device = (NMDevice *)g_ptr_array_index(devices, i);
        if (NM_IS_DEVICE_WIFI(device))
        {
            wifi_status->wifi_device = device;
            break;
        }
    }
    if (wifi_status->wifi_device == NULL)
    {
        g_print("Could not find WiFi device\n");
        return false;
    }
    return true;
}
static gboolean check_wifi_status(gpointer user_data)
{
    const int MAX_ATTEMPTS = 10;
    WifiStatus *wifi_status = (WifiStatus *)user_data;
    wifi_status->wifi_enabled = nm_client_wireless_get_enabled(wifi_status->client);
    wifi_status->attempts++;
    if (wifi_status->wifi_enabled == nm_client_wireless_get_enabled(wifi_status->client) || wifi_status->attempts >= MAX_ATTEMPTS)
    {
        g_main_loop_quit(wifi_status->loop);
    }

    return wifi_status->wifi_enabled == nm_client_wireless_get_enabled(wifi_status->client) && wifi_status->attempts < MAX_ATTEMPTS;
}

bool set_wifi_enabled_with_attempts(bool enable)
{
    GError *error = NULL;
    NMClient *client;
    GMainLoop *loop = g_main_loop_new(NULL, FALSE);
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        g_print("Could not connect to NetworkManager: %s\n", error->message);
        g_error_free(error);
        return false;
    }
    WifiStatus wifi_status = {enable, client, loop, NULL, 0};
    nm_client_wireless_set_enabled(client, enable);
    g_timeout_add_seconds(1, check_wifi_status, &wifi_status);
    g_main_loop_run(loop);
    bool result = wifi_status.wifi_enabled == enable;
    g_object_unref(client);
    g_main_loop_unref(loop);
    return result;
}
void on_deactivate_connection_finished(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
    GError *error = NULL;
    gboolean success = nm_client_deactivate_connection_finish(NM_CLIENT(source_object), res, &error);
    if (!success)
    {
        g_print("Error deactivating connection: %s\n", error->message);
        g_error_free(error);
    }

    GMainLoop *loop = (GMainLoop *)user_data;
    g_main_loop_quit(loop);
}

bool disconnect_wifi()
{
    GError *error = NULL;
    NMClient *client = NULL;
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        g_print("Could not connect to NetworkManager: %s\n", error->message);
        g_error_free(error);
        return false;
    }

    const GPtrArray *devices = nm_client_get_devices(client);
    if (devices == NULL)
    {
        g_print("Could not get devices: %s\n", error->message);
        g_error_free(error);
        return false;
    }
    NMDevice *wifi_device = NULL;
    NMActiveConnection *active_connection = NULL;
    for (guint i = 0; i < devices->len; i++)
    {
        NMDevice *device = (NMDevice *)g_ptr_array_index(devices, i);
        if (NM_IS_DEVICE_WIFI(device))
        {
            wifi_device = device;
            active_connection = nm_device_get_active_connection(wifi_device);
            break;
        }
    }

    if (active_connection == NULL)
    {
        g_print("No active WiFi connection found\n");
        return true;
    }
    GMainLoop *loop = g_main_loop_new(NULL, FALSE);
    nm_client_deactivate_connection_async(client, active_connection, NULL, on_deactivate_connection_finished, loop);
    g_main_loop_run(loop);
    g_main_loop_unref(loop);

    return true;
}
bool isWifiActive()
{
    NMClient *client = NULL;
    GError *error = NULL;
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        std::cout << "Error: " << error->message << std::endl;
        g_error_free(error);
        return false;
    }
    if (nm_client_wireless_get_enabled(client))
    {
        g_object_unref(client);
        return true;
    }
    else
    {
        std::cout << "Wi-Fi is not not active." << std::endl;
        g_object_unref(client);
        return false;
    }
}

int getSignalLevel(int &rssi, int &band)
{
    // values come from wifi_internal_msg.h
    constexpr int rssi_levels_2g[] = {-88, -82, -75, -65};
    constexpr int rssi_levels_5g[] = {-85, -79, -72, -65};

    int level = 0;
    switch (band)
    {
    case BandType::BAND_2GHZ:
        for (int i = 0; i < 4; i++)
        {
            if (rssi < rssi_levels_2g[i])
            {
                break;
            }
            level++;
        }
        break;
    case BandType::BAND_5GHZ:
        for (int i = 0; i < 4; i++)
        {
            if (rssi < rssi_levels_5g[i])
            {
                break;
            }
            level++;
        }
        break;
    case BandType::BAND_NONE:
    case BandType::BAND_ANY:
    default:
        break;
    }
    return level;
}

int main()
{
    if (isWifiActive())
    {
        std::cout << "Wi-Fi is active." << std::endl;
    }
    else
    {
        std::cout << "Wi-Fi is not active." << std::endl;
    }

    bool enable_result = set_wifi_enabled_with_attempts(true);
    if (enable_result)
    {
        g_print("Wi-Fi enabled\n");
    }
    else
    {
        g_print("Failed to enable Wi-Fi\n");
    }

    bool disable_result = set_wifi_enabled_with_attempts(false);
    if (disable_result)
    {
        g_print("Wi-Fi disabled\n");
    }
    else
    {
        g_print("Failed to disable Wi-Fi\n");
    }

    bool enable_result_again = set_wifi_enabled_with_attempts(true);
    if (enable_result_again)
    {
        g_print("Wi-Fi enabled again\n");
    }
    else
    {
        g_print("Failed to enable Wi-Fi again\n");
    }
    sleep(2);
    WifiStatus wifi_status={};
    wifi_status.wifi_enabled = init_wifi_device(&wifi_status);
    if (wifi_status.wifi_enabled)
    {
        if (scan_wifi(&wifi_status))
        {
            std::vector<AccessPointInfo> ap_info_list = get_scan_info(&wifi_status);
            for (const auto &ap_info : ap_info_list)
            {
                std::cout << "SSID: " << ap_info.ssid << "\n";
                std::cout << "BSSID: " << ap_info.bssid << "\n";
                std::cout << "RSSI: " << ap_info.rssi << "/100\n";
                std::cout << "Frequency: " << ap_info.frequency << " MHz\n";
                std::cout << "Band: " << ap_info.band << " GHz\n";
                std::cout << "Security: " << ap_info.security << "\n\n";
            }
        }
        else
        {
            g_print("Scan failed\n");
        }
    }
    int rssi = 1;
    int band = 1;
    int level = getSignalLevel(rssi, band);
    // expected level 4.
    std::cout << "RSSI: " << rssi << ", Band: " << static_cast<int>(band) << ", Signal Level: " << level << std::endl;
    bool result = disconnect_wifi();
    if (result)
    {
        g_print("WiFi connection disconnected!\n");
    }
    else
    {
        g_print("Could not disconnect WiFi connection.\n");
    }

    return 0;
}
