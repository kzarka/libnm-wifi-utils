// wifi_utils.h

#ifndef WIFI_UTILS_H
#define WIFI_UTILS_H

#include <iostream>
#include <NetworkManager.h>
#include <algorithm>
#include <arpa/inet.h>
#include <cstring>
#include <iomanip>
#include <map>
#include <sstream>
#include <string>
#include <vector>


// these are taken from ip_tools.h
static const int MIN_PREFIX_LEN = 0;
static const int MAX_PREFIX_LEN = 32;
static const int MAX_IPV6_PREFIX_LEN = 128;
static const int BIT_NUM_PER_BYTE = 8;
static const int IPV4_BYTE_NUM = 4;
static const unsigned int IPV4_DOT_NUM = 3;
static const int IPV6_BYTE_NUM = 16;
static const int IPV6_DIGIT_NUM_PER_SEG = 4;
static const int IPV6_COLON_NUM = 7;
static const int MAX_IPV4_MASK_BYTE = 255;
static const int POS_0 = 0;
static const int POS_1 = 1;
static const int POS_2 = 2;
static const int POS_3 = 3;
static const int HEX_BYTE_DIGIT_NUM = 2;
static const int HEX_FORM = 16;
static const int MIN_BYTE = 0;
static const int MAX_BYTE = 255;
static const unsigned int BIT_NUM_BYTE = 8;
static const int BITS_24 = 24;
static const int BITS_16 = 16;
static const int BITS_8 = 8;

enum WifiSecurityType
{
    WIFI_SEC_TYPE_INVALID = 0,
    WIFI_SEC_TYPE_OPEN = 1,
    WIFI_SEC_TYPE_WEP = 2,
    WIFI_SEC_TYPE_PSK = 3,
    WIFI_SEC_TYPE_SAE = 4,
};

#define IPV4_ADDRESS_TYPE 1
#define IPV6_ADDRESS_TYPE 0
struct IpConfigV4
{
    int ipAddress = 0;
    int gateway = 0;
};

struct WifiDeviceConfigV4
{
    std::string ssid;
    std::string bssid;
    std::string preSharedKey;
    std::string isHiddenSsid;
    WifiSecurityType securityType;
    int netId;
    int ipType;
    int creatorUid;
    int disableReason;
    int randomMacType;
    std::string randomMacAddr = {};
    IpConfigV4 staticIp{}; // Default-initialized IpConfig
};
struct IpConfigV6
{
    std::vector<unsigned char> ipAddress;
    int gateway;
    IpConfigV6()
    {
        // Set default values
        ipAddress = {}; // Empty vector
        gateway = 0;
    }
};
struct WifiDeviceConfigV6
{
    std::string ssid;
    std::string bssid;
    std::string preSharedKey;
    std::string isHiddenSsid;
    WifiSecurityType securityType;
    int netId;
    int ipType;
    int creatorUid;
    int disableReason;
    int randomMacType;
    std::string randomMacAddr;
    IpConfigV6 staticIp{};
};

void print_connection_info(NMConnection *connection);
NMConnection *create_wifi_connectionV4(const WifiDeviceConfigV4 &config);
NMConnection *create_wifi_connectionV6(const WifiDeviceConfigV6 &config);
void add_connection(NMClient *client, NMConnection *connection);
void on_add_connection_finish(GObject *source, GAsyncResult *res, gpointer user_data);
NMClient *init_nm_client();

#endif // WIFI_UTILS_H
