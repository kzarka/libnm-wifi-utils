#include <iostream>
#include <string>
#include <vector>

enum BandType
{
    BAND_NONE = 0, /* unknown */
    BAND_2GHZ = 1, /* 2.4GHz */
    BAND_5GHZ = 2, /* 5GHz */
    BAND_ANY = 3   /* Dual-mode frequency band */
};

int getSignalLevel(int &rssi,  int &band)
{
    constexpr int RSSI_LEVEL_1_2G = -88;
    constexpr int RSSI_LEVEL_2_2G = -82;
    constexpr int RSSI_LEVEL_3_2G = -75;
    constexpr int RSSI_LEVEL_4_2G = -65;

    constexpr int RSSI_LEVEL_1_5G = -85;
    constexpr int RSSI_LEVEL_2_5G = -79;
    constexpr int RSSI_LEVEL_3_5G = -72;
    constexpr int RSSI_LEVEL_4_5G = -65;

    constexpr int rssi_levels_2g[] = {RSSI_LEVEL_1_2G, RSSI_LEVEL_2_2G, RSSI_LEVEL_3_2G, RSSI_LEVEL_4_2G};
    constexpr int rssi_levels_5g[] = {RSSI_LEVEL_1_5G, RSSI_LEVEL_2_5G, RSSI_LEVEL_3_5G, RSSI_LEVEL_4_5G};

    int level = 0;
    switch (band)
    {
    case BandType::BAND_2GHZ:
        for (int i = 0; i < 4; i++)
        {
            if (rssi < rssi_levels_2g[i])
            {
                break;
            }
            level++;
        }
        break;
    case BandType::BAND_5GHZ:
        for (int i = 0; i < 4; i++)
        {
            if (rssi < rssi_levels_5g[i])
            {
                break;
            }
            level++;
        }
        break;
    case BandType::BAND_NONE:
    case BandType::BAND_ANY:
    default:
        break;
    }
    return level;
}

int main()
{
    int rssi = 1;
    int band = 1;
    int level = getSignalLevel(rssi, band);
    // expected level 4.
    std::cout << "RSSI: " << rssi << ", Band: " << static_cast<int>(band) << ", Signal Level: " << level << std::endl;
    return 0;
}