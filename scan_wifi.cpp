#include <NetworkManager.h>
#include <glib.h>
#include <iostream>
#include <vector>

enum BandType
{
    BAND_NONE = 0, /* unknown */
    BAND_2GHZ = 1, /* 2.4GHz */
    BAND_5GHZ = 2, /* 5GHz */
    BAND_ANY = 3   /* Dual-mode frequency band */
};
struct AccessPointInfo
{
    std::string ssid;
    std::string bssid;
    int rssi;
    int frequency;
    BandType band;
    std::string security;
};
struct WifiStatus
{
    bool wifi_enabled;
    NMClient *client;
    GMainLoop *loop;
    NMDevice *wifi_device;
    int attempts;
};

BandType get_band(guint32 frequency)
{
    if (frequency >= 2400 && frequency <= 2495)
    {
        return BAND_2GHZ;
    }
    else if (frequency >= 5150 && frequency <= 5925)
    {
        return BAND_5GHZ;
    }
    else
    {
        return BAND_NONE;
    }
}
const char *get_wifi_security_type(NMAccessPoint *ap)
{
    NM80211ApSecurityFlags wpa_flags = nm_access_point_get_wpa_flags(ap);
    NM80211ApSecurityFlags rsn_flags = nm_access_point_get_rsn_flags(ap);

    if (wpa_flags & NM_802_11_AP_SEC_KEY_MGMT_PSK || rsn_flags & NM_802_11_AP_SEC_KEY_MGMT_PSK)
    {
        return "WPA-PSK";
    }
    else if (wpa_flags & NM_802_11_AP_SEC_KEY_MGMT_802_1X || rsn_flags & NM_802_11_AP_SEC_KEY_MGMT_802_1X)
    {
        return "WPA-Enterprise";
    }
    else if (wpa_flags == 0 && rsn_flags == 0)
    {
        return "None";
    }
    else
    {
        return "Unknown";
    }
}
void on_wifi_scan_finished(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
    WifiStatus *wifi_status = (WifiStatus *)user_data;
    GError *error = NULL;
    gboolean success = nm_device_wifi_request_scan_finish(NM_DEVICE_WIFI(source_object), res, &error);
    if (!success)
    {
        if (g_strrstr(error->message, "Scanning not allowed") != NULL)
        {
            // Scanning not allowed immediately following previous scan
            // Retry after a short delay
            g_print("Scanning not allowed, retrying in 4 seconds...\n");
            g_error_free(error);
            sleep(4);

            // Check if attempts is less than 10
            if (wifi_status->attempts < 10)
            {
                wifi_status->attempts++; // Increment attempts
                nm_device_wifi_request_scan_async(NM_DEVICE_WIFI(source_object), NULL, on_wifi_scan_finished, wifi_status);
            }
            else
            {
                g_print("Reached maximum retries, terminating...\n");
            }

            return;
        }

        g_print("Error scanning for WiFi networks: %s\n", error->message);
        g_error_free(error);
    }
    GMainLoop *loop = wifi_status->loop;
    g_main_loop_quit(loop);
}

bool scan_wifi(WifiStatus *wifi_status)
{
    GMainLoop *loop = g_main_loop_new(NULL, FALSE);
    wifi_status->loop = loop;
    nm_device_wifi_request_scan_async(NM_DEVICE_WIFI(wifi_status->wifi_device), NULL, on_wifi_scan_finished, wifi_status);
    g_main_loop_run(loop);
    g_main_loop_unref(loop);
    return true;
}
std::vector<AccessPointInfo> get_scan_info(WifiStatus *wifi_status)
{
    std::vector<AccessPointInfo> ap_info_list;

    if (wifi_status->wifi_device != NULL)
    {
        const GPtrArray *aps = nm_device_wifi_get_access_points(NM_DEVICE_WIFI(wifi_status->wifi_device));
        if (aps != NULL)
        {
            for (guint i = 0; i < aps->len; i++)
            {
                NMAccessPoint *ap = (NMAccessPoint *)g_ptr_array_index(aps, i);
                GBytes *ssid_bytes = nm_access_point_get_ssid(ap);
                if (ssid_bytes != NULL)
                {
                    AccessPointInfo ap_info;

                    gsize ssid_len;
                    const gchar *ssid_str = (const gchar *)g_bytes_get_data(ssid_bytes, &ssid_len);
                    ap_info.ssid.assign(ssid_str, ssid_len);
                    g_bytes_unref(ssid_bytes);

                    ap_info.bssid = nm_access_point_get_bssid(ap);
                    ap_info.rssi = nm_access_point_get_strength(ap);
                    ap_info.frequency = nm_access_point_get_frequency(ap);
                    ap_info.band = get_band(nm_access_point_get_frequency(ap));
                    ap_info.security = get_wifi_security_type(ap);

                    ap_info_list.push_back(ap_info);
                }
            }
        }
    }

    return ap_info_list;
}
bool init_wifi_device(WifiStatus *wifi_status)
{
    GError *error = NULL;
    NMClient *client = NULL;
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        g_print("Could not connect to NetworkManager: %s\n", error->message);
        g_error_free(error);
        return false;
    }
    wifi_status->client = client;

    const GPtrArray *devices = nm_client_get_devices(client);
    if (devices == NULL)
    {
        g_print("Could not get devices: %s\n", error->message);
        g_error_free(error);
        return false;
    }

    for (guint i = 0; i < devices->len; i++)
    {
        NMDevice *device = (NMDevice *)g_ptr_array_index(devices, i);
        if (NM_IS_DEVICE_WIFI(device))
        {
            wifi_status->wifi_device = device;
            break;
        }
    }
    if (wifi_status->wifi_device == NULL)
    {
        g_print("Could not find WiFi device\n");
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
    WifiStatus wifi_status;
    wifi_status.wifi_enabled = init_wifi_device(&wifi_status);
    wifi_status.attempts = 0; // Initialize attempts to 0
    if (wifi_status.wifi_enabled)
    {
        if (scan_wifi(&wifi_status))
        {
            std::vector<AccessPointInfo> ap_info_list = get_scan_info(&wifi_status);
            for (const auto &ap_info : ap_info_list)
            {
                std::cout << "SSID: " << ap_info.ssid << "\n";
                std::cout << "BSSID: " << ap_info.bssid << "\n";
                std::cout << "RSSI: " << ap_info.rssi << "/100\n";
                std::cout << "Frequency: " << ap_info.frequency << " MHz\n";
                std::cout << "Band: " << ap_info.band << " GHz\n";
                std::cout << "Security: " << ap_info.security << "\n\n";
            }
        }
        else
        {
            g_print("Scan failed\n");
        }
    }
    return 0;
}