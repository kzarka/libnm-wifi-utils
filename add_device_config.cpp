#include <iostream>
#include <NetworkManager.h>
#include <algorithm>
#include <arpa/inet.h>
#include <cstring>
#include <iomanip>
#include <map>
#include <sstream>
#include <string>
#include <vector>

// these are taken from ip_tools.h
static const int MIN_PREFIX_LEN = 0;
static const int MAX_PREFIX_LEN = 32;
static const int MAX_IPV6_PREFIX_LEN = 128;
static const int BIT_NUM_PER_BYTE = 8;
static const int IPV4_BYTE_NUM = 4;
static const unsigned int IPV4_DOT_NUM = 3;
static const int IPV6_BYTE_NUM = 16;
static const int IPV6_DIGIT_NUM_PER_SEG = 4;
static const int IPV6_COLON_NUM = 7;
static const int MAX_IPV4_MASK_BYTE = 255;
static const int POS_0 = 0;
static const int POS_1 = 1;
static const int POS_2 = 2;
static const int POS_3 = 3;
static const int HEX_BYTE_DIGIT_NUM = 2;
static const int HEX_FORM = 16;
static const int MIN_BYTE = 0;
static const int MAX_BYTE = 255;
static const unsigned int BIT_NUM_BYTE = 8;
static const int BITS_24 = 24;
static const int BITS_16 = 16;
static const int BITS_8 = 8;

enum WifiSecurityType
{
    WIFI_SEC_TYPE_INVALID = 0,
    WIFI_SEC_TYPE_OPEN = 1,
    WIFI_SEC_TYPE_WEP = 2,
    WIFI_SEC_TYPE_PSK = 3,
    WIFI_SEC_TYPE_SAE = 4,
};

#define IPV4_ADDRESS_TYPE 1
#define IPV6_ADDRESS_TYPE 0
struct IpConfigV4
{
    int ipAddress = 0;
    int gateway = 0;
};

struct WifiDeviceConfigV4
{
    std::string ssid;
    std::string bssid;
    std::string preSharedKey;
    std::string isHiddenSsid;
    WifiSecurityType securityType;
    int netId;
    int ipType;
    int creatorUid;
    int disableReason;
    int randomMacType;
    std::string randomMacAddr = {};
    IpConfigV4 staticIp{}; // Default-initialized IpConfig
};
struct IpConfigV6
{
    std::vector<unsigned char> ipAddress;
    int gateway;
    IpConfigV6()
    {
        // Set default values
        ipAddress = {}; // Empty vector
        gateway = 0;
    }
};
struct WifiDeviceConfigV6
{
    std::string ssid;
    std::string bssid;
    std::string preSharedKey;
    std::string isHiddenSsid;
    WifiSecurityType securityType;
    int netId;
    int ipType;
    int creatorUid;
    int disableReason;
    int randomMacType;
    std::string randomMacAddr;
    IpConfigV6 staticIp{}; // Default-initialized IpConfig
};

std::string ConvertIpv4Address(unsigned int addressIpv4)
{
    std::string address;
    if (addressIpv4 == 0)
    {
        return address;
    }

    std::ostringstream stream;
    stream << ((addressIpv4 >> BITS_24) & 0xFF) << "." << ((addressIpv4 >> BITS_16) & 0xFF) << "."
           << ((addressIpv4 >> BITS_8) & 0xFF) << "." << (addressIpv4 & 0xFF);
    address = stream.str();

    return address;
}

unsigned int ConvertIpv4Address(const std::string &address)
{
    std::string tmpAddress = address;
    int addrInt = 0;
    unsigned int i = 0;
    for (i = 0; i < IPV4_DOT_NUM; i++)
    {
        std::string::size_type npos = tmpAddress.find(".");
        if (npos == std::string::npos)
        {
            break;
        }
        std::string value = tmpAddress.substr(0, npos);
        unsigned int tmp = std::atoi(value.c_str());
        if ((tmp < MIN_BYTE) || (tmp > MAX_BYTE))
        {
            break;
        }
        addrInt += tmp << ((IPV4_DOT_NUM - i) * BIT_NUM_BYTE);
        tmpAddress = tmpAddress.substr(npos + 1);
    }

    if (i != IPV4_DOT_NUM)
    {
        return 0;
    }
    int tmp = std::atoi(tmpAddress.c_str());
    if ((tmp < MIN_BYTE) || (tmp > MAX_BYTE))
    {
        return 0;
    }
    addrInt += tmp;

    return addrInt;
}

std::string ConvertIpv6Address(const std::vector<unsigned char> &addressIpv6)
{
    std::string address;
    if (addressIpv6.size() != IPV6_BYTE_NUM)
    {
        return address;
    }

    std::ostringstream stream;
    stream << std::hex << std::setw(POS_2) << std::setfill('0') << static_cast<int>(addressIpv6[0]);
    stream << std::hex << std::setw(POS_2) << std::setfill('0') << static_cast<int>(addressIpv6[1]);
    for (int i = POS_2; i < IPV6_BYTE_NUM; i += POS_2)
    {
        stream << ":";
        stream << std::hex << std::setw(POS_2) << std::setfill('0') << static_cast<int>(addressIpv6[i]);
        stream << std::hex << std::setw(POS_2) << std::setfill('0') << static_cast<int>(addressIpv6[i + 1]);
    }
    address = stream.str();

    return address;
}
void ConvertIpv6Address(const std::string &address, std::vector<unsigned char> &addressIpv6)
{
    std::string tmpAddress = address;
    addressIpv6.clear();
    std::vector<unsigned char> ipv6;
    int i = 0;
    for (i = 0; i < IPV6_COLON_NUM; i++)
    {
        std::string::size_type npos = tmpAddress.find(":");
        if (npos == std::string::npos)
        {
            break;
        }

        std::string value = tmpAddress.substr(0, npos);
        if (value.size() != IPV6_DIGIT_NUM_PER_SEG)
        {
            break;
        }
        ipv6.push_back(std::stoi(value.substr(POS_0, HEX_BYTE_DIGIT_NUM), nullptr, HEX_FORM));
        ipv6.push_back(std::stoi(value.substr(POS_2, HEX_BYTE_DIGIT_NUM), nullptr, HEX_FORM));
        tmpAddress = tmpAddress.substr(npos + 1);
    }

    if (i != IPV6_COLON_NUM)
    {
        return;
    }
    if (tmpAddress.size() != IPV6_DIGIT_NUM_PER_SEG)
    {
        return;
    }
    ipv6.push_back(std::stoi(tmpAddress.substr(POS_0, HEX_BYTE_DIGIT_NUM), nullptr, HEX_FORM));
    ipv6.push_back(std::stoi(tmpAddress.substr(POS_2, HEX_BYTE_DIGIT_NUM), nullptr, HEX_FORM));

    addressIpv6.assign(ipv6.begin(), ipv6.end());
    return;
}

NMClient *init_nm_client()
{
    GError *error = NULL;
    NMClient *client = nm_client_new(NULL, &error);

    if (!client)
    {
        std::cerr << "Error: " << error->message << std::endl;
        g_error_free(error);
        exit(1);
    }
    return client;
}

NMConnection *create_wifi_connectionV4(const WifiDeviceConfigV4 &wifiDeviceConfigV4)
{
    GError *error = NULL;
    // Create a new NMConnection object
    NMConnection *connection = nm_simple_connection_new();
    // Configure the connection settings
    NMSetting *s_con = NM_SETTING(nm_setting_connection_new());
    g_object_set(G_OBJECT(s_con),
                 NM_SETTING_CONNECTION_ID, "My WiFi Connection",
                 NM_SETTING_CONNECTION_UUID, nm_utils_uuid_generate(),
                 NM_SETTING_CONNECTION_TYPE, NM_SETTING_WIRELESS_SETTING_NAME,
                 NM_SETTING_CONNECTION_AUTOCONNECT, FALSE,
                 NULL);
    nm_connection_add_setting(connection, s_con);
    // Add the WiFi settings
    NMSetting *s_wifi = NM_SETTING(nm_setting_wireless_new());
    int ssidSize = wifiDeviceConfigV4.ssid.size();
    GBytes *ssid = g_bytes_new(wifiDeviceConfigV4.ssid.c_str(), ssidSize);

    // Set the BSSID if its not hiddend
    // Note: If the set and reported BSSIDs are random numbers without corresponding access points,
    // NetworkManager may ignore the provided BSSID and automatically select an available access point based on the SSID.
    if (wifiDeviceConfigV4.isHiddenSsid == "false")
    {
        g_object_set(G_OBJECT(s_wifi),
                     NM_SETTING_WIRELESS_SSID, ssid,
                     NM_SETTING_WIRELESS_MODE, NM_SETTING_WIRELESS_MODE_INFRA,
                     NM_SETTING_WIRELESS_BSSID, wifiDeviceConfigV4.bssid.c_str(),
                     NULL);
    }
    // manually setting the mac
    if (!wifiDeviceConfigV4.randomMacAddr.empty())
    {
        g_object_set(G_OBJECT(s_wifi),
                     NM_SETTING_WIRELESS_SSID, ssid,
                     NM_SETTING_WIRELESS_MODE, NM_SETTING_WIRELESS_MODE_INFRA,
                     NM_SETTING_WIRELESS_BSSID, nm_utils_hwaddr_atoba(wifiDeviceConfigV4.randomMacAddr.c_str(), ETH_ALEN),
                     NULL);
    }

    g_bytes_unref(ssid);
    nm_connection_add_setting(connection, s_wifi);

    // Add the WiFi security settings
    NMSetting *s_wsec = NM_SETTING(nm_setting_wireless_security_new());

    switch (wifiDeviceConfigV4.securityType)
    {
    case WifiSecurityType::WIFI_SEC_TYPE_OPEN:
        g_object_set(G_OBJECT(s_wsec),
                     NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "none",
                     NULL);
        break;

    case WifiSecurityType::WIFI_SEC_TYPE_WEP:
        g_object_set(G_OBJECT(s_wsec),
                     NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "ieee8021x",
                     NM_SETTING_WIRELESS_SECURITY_WEP_KEY_TYPE, NM_SETTING_WIRELESS_SECURITY_WEP_KEY_TYPE,
                     NM_SETTING_WIRELESS_SECURITY_WEP_KEY0, wifiDeviceConfigV4.preSharedKey.c_str(),
                     NULL);
        break;

    case WifiSecurityType::WIFI_SEC_TYPE_PSK:
        g_object_set(G_OBJECT(s_wsec),
                     NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "wpa-psk",
                     NM_SETTING_WIRELESS_SECURITY_PSK, wifiDeviceConfigV4.preSharedKey.c_str(),
                     NULL);
        break;

    case WifiSecurityType::WIFI_SEC_TYPE_SAE:
        g_object_set(G_OBJECT(s_wsec),
                     NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "sae",
                     NM_SETTING_WIRELESS_SECURITY_PSK, wifiDeviceConfigV4.preSharedKey.c_str(),
                     NULL);
        break;

    default:
        g_object_unref(s_wsec);
        return NULL;
    }
    // manual ip
    if (wifiDeviceConfigV4.staticIp.ipAddress != 0)
    {
        NMSetting *s_ip4 = NM_SETTING(nm_setting_ip4_config_new());
        g_object_set(G_OBJECT(s_ip4),
                     NM_SETTING_IP_CONFIG_METHOD, NM_SETTING_IP4_CONFIG_METHOD_MANUAL,
                     NULL);
        // Add IP4 address
        std::string ip_string = ConvertIpv4Address(wifiDeviceConfigV4.staticIp.ipAddress);
        const char *ip_char_ptr = ip_string.c_str();
        NMIPAddress *address = nm_ip_address_new(AF_INET, ip_char_ptr, 24, &error);
        nm_setting_ip_config_add_address(NM_SETTING_IP_CONFIG(s_ip4), address);
        nm_ip_address_unref(address);
        nm_connection_add_setting(connection, s_ip4);
    }
    // dynamic ip
    else
    {
        NMSetting *s_ip4 = NM_SETTING(nm_setting_ip4_config_new());

        g_object_set(G_OBJECT(s_ip4),
                     NM_SETTING_IP_CONFIG_METHOD, NM_SETTING_IP4_CONFIG_METHOD_AUTO,
                     NULL);

        nm_connection_add_setting(connection, s_ip4);
    }

    if (error)
    {
        std::cerr << "Error: " << error->message << std::endl;
        g_error_free(error);
    }

    return connection;
}

NMConnection *create_wifi_connectionV6(const WifiDeviceConfigV6 &wifiDeviceConfigV6)
{
    GError *error = NULL;
    // Create a new NMConnection object
    NMConnection *connection = nm_simple_connection_new();

    // Configure the connection settings
    NMSetting *s_con = NM_SETTING(nm_setting_connection_new());
    g_object_set(G_OBJECT(s_con),
                 NM_SETTING_CONNECTION_ID, "My WiFi Connection",
                 NM_SETTING_CONNECTION_UUID, nm_utils_uuid_generate(),
                 NM_SETTING_CONNECTION_TYPE, NM_SETTING_WIRELESS_SETTING_NAME,
                 NM_SETTING_CONNECTION_AUTOCONNECT, FALSE,
                 NULL);
    nm_connection_add_setting(connection, s_con);

    // Add the WiFi settings
    NMSetting *s_wifi = NM_SETTING(nm_setting_wireless_new());
    int ssidSize = wifiDeviceConfigV6.ssid.size();
    GBytes *ssid = g_bytes_new(wifiDeviceConfigV6.ssid.c_str(), ssidSize);
    // Set the BSSID if its not hiddend
    // Note: If the set and reported BSSIDs are random numbers without corresponding access points,
    // NetworkManager may ignore the provided BSSID and automatically select an available access point based on the SSID.
    if (wifiDeviceConfigV6.isHiddenSsid == "false")
    {
        g_object_set(G_OBJECT(s_wifi),
                     NM_SETTING_WIRELESS_SSID, ssid,
                     NM_SETTING_WIRELESS_MODE, NM_SETTING_WIRELESS_MODE_INFRA,
                     NM_SETTING_WIRELESS_BSSID, wifiDeviceConfigV6.bssid.c_str(),
                     NULL);
    }
    // manually setting the mac
    if (!wifiDeviceConfigV6.randomMacAddr.empty())
    {
        g_object_set(G_OBJECT(s_wifi),
                     NM_SETTING_WIRELESS_SSID, ssid,
                     NM_SETTING_WIRELESS_MODE, NM_SETTING_WIRELESS_MODE_INFRA,
                     NM_SETTING_WIRELESS_BSSID, nm_utils_hwaddr_atoba(wifiDeviceConfigV6.randomMacAddr.c_str(), ETH_ALEN),
                     NULL);
    }

    g_bytes_unref(ssid);
    nm_connection_add_setting(connection, s_wifi);

    // Add the WiFi security settings
    NMSetting *s_wsec = NM_SETTING(nm_setting_wireless_security_new());

    switch (wifiDeviceConfigV6.securityType)
    {
    case WifiSecurityType::WIFI_SEC_TYPE_OPEN:
        g_object_set(G_OBJECT(s_wsec),
                     NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "none",
                     NULL);
        break;

    case WifiSecurityType::WIFI_SEC_TYPE_WEP:
        g_object_set(G_OBJECT(s_wsec),
                     NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "ieee8021x",
                     NM_SETTING_WIRELESS_SECURITY_WEP_KEY_TYPE, NM_SETTING_WIRELESS_SECURITY_WEP_KEY_TYPE,
                     NM_SETTING_WIRELESS_SECURITY_WEP_KEY0, wifiDeviceConfigV6.preSharedKey.c_str(),
                     NULL);
        break;

    case WifiSecurityType::WIFI_SEC_TYPE_PSK:
        g_object_set(G_OBJECT(s_wsec),
                     NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "wpa-psk",
                     NM_SETTING_WIRELESS_SECURITY_PSK, wifiDeviceConfigV6.preSharedKey.c_str(),
                     NULL);
        break;

    case WifiSecurityType::WIFI_SEC_TYPE_SAE:
        g_object_set(G_OBJECT(s_wsec),
                     NM_SETTING_WIRELESS_SECURITY_KEY_MGMT, "sae",
                     NM_SETTING_WIRELESS_SECURITY_PSK, wifiDeviceConfigV6.preSharedKey.c_str(),
                     NULL);
        break;

    default:
        g_object_unref(s_wsec);
        return NULL;
    }
    // manual ip
    if (wifiDeviceConfigV6.staticIp.ipAddress.empty())
    {

        NMSetting *s_ip6 = NM_SETTING(nm_setting_ip6_config_new());

        g_object_set(G_OBJECT(s_ip6),
                     NM_SETTING_IP_CONFIG_METHOD, NM_SETTING_IP6_CONFIG_METHOD_MANUAL,
                     NULL);

        // Add IP6 address
        std::string ip_string = ConvertIpv4Address(1);
        const char *ip_char_ptr = ip_string.c_str();
        NMIPAddress *address = nm_ip_address_new(AF_INET6, ip_char_ptr, 64, &error);
        nm_setting_ip_config_add_address(NM_SETTING_IP_CONFIG(s_ip6), address);
        nm_ip_address_unref(address);
        nm_connection_add_setting(connection, s_ip6);
    }
    // dynamic ip
    else
    {
        NMSetting *s_ip6 = NM_SETTING(nm_setting_ip6_config_new());
        g_object_set(G_OBJECT(s_ip6),
                     NM_SETTING_IP_CONFIG_METHOD, NM_SETTING_IP4_CONFIG_METHOD_AUTO,
                     NULL);
        nm_connection_add_setting(connection, s_ip6);
    }

    if (error)
    {
        std::cerr << "Error: " << error->message << std::endl;
        g_error_free(error);
    }

    return connection;
}

void on_add_connection_finish(GObject *source, GAsyncResult *res, gpointer user_data)
{
    GError *error = NULL;
    NMRemoteConnection *remote = nm_client_add_connection_finish(NM_CLIENT(source), res, &error);

    if (!remote)
    {
        std::cerr << "Error: " << error->message << std::endl;
        g_error_free(error);
    }
    else
    {
        std::cout << "Successfully added WiFi connection" << std::endl;
        g_object_unref(remote);
    }

    GMainLoop *main_loop = static_cast<GMainLoop *>(user_data);
    g_main_loop_quit(main_loop);
}

void add_connection(NMClient *client, NMConnection *connection)
{
    GMainLoop *main_loop = g_main_loop_new(NULL, FALSE);

    nm_client_add_connection_async(
        client, connection, TRUE, NULL, on_add_connection_finish, main_loop);

    g_main_loop_run(main_loop);
    g_main_loop_unref(main_loop);
}

void print_connection_info(NMConnection *connection)
{
    NMSettingConnection *s_con;
    NMSettingWireless *s_wifi;
    NMSettingWirelessSecurity *s_wsec;
    NMSettingIP4Config *s_ip4;
    NMSettingIP6Config *s_ip6;

    s_con = NM_SETTING_CONNECTION(nm_connection_get_setting(connection, NM_TYPE_SETTING_CONNECTION));
    s_wifi = NM_SETTING_WIRELESS(nm_connection_get_setting(connection, NM_TYPE_SETTING_WIRELESS));
    s_wsec = NM_SETTING_WIRELESS_SECURITY(nm_connection_get_setting(connection, NM_TYPE_SETTING_WIRELESS_SECURITY));
    s_ip4 = NM_SETTING_IP4_CONFIG(nm_connection_get_setting(connection, NM_TYPE_SETTING_IP4_CONFIG));
    s_ip6 = NM_SETTING_IP6_CONFIG(nm_connection_get_setting(connection, NM_TYPE_SETTING_IP6_CONFIG));

    if (s_con)
    {
        std::cout << "Connection ID: " << nm_setting_connection_get_id(s_con) << std::endl;
    }
    if (s_wifi)
    {
        GBytes *ssid = nm_setting_wireless_get_ssid(s_wifi);
        if (ssid)
        {
            gsize ssid_len = 0;
            const char *ssid_data = (const char *)g_bytes_get_data(ssid, &ssid_len);
            std::string ssid_str(ssid_data, ssid_len);
            std::cout << "SSID: " << ssid_str << std::endl;
        }
    }
    const char *bssid = nm_setting_wireless_get_bssid(s_wifi);
    if (bssid)
    {
        std::string bssid_string = nm_utils_hwaddr_ntoa(bssid, ETH_ALEN);
        std::cout << "BSSID: " << bssid_string << std::endl;
    }

    if (s_wsec)
    {
        std::cout << "Key management: " << nm_setting_wireless_security_get_key_mgmt(s_wsec) << std::endl;
        std::cout << "PSK: " << nm_setting_wireless_security_get_psk(s_wsec) << std::endl;
    }
    // ip will only will be printed if assgined manually , otherwise it only assigned when connection is established..
    if (s_ip4)
    {
        guint num_addresses = nm_setting_ip_config_get_num_addresses(NM_SETTING_IP_CONFIG(s_ip4));
        if (num_addresses > 0)
        {
            NMIPAddress *address = nm_setting_ip_config_get_address(NM_SETTING_IP_CONFIG(s_ip4), 0);
            std::cout << "IP address: " << ConvertIpv4Address(nm_ip_address_get_address(address)) << std::endl;
        }
    }
    // ip will only will be printed if assgined manually , otherwise it only assigned when connection is established..
    if (s_ip6)
    {
        guint num_addresses = nm_setting_ip_config_get_num_addresses(NM_SETTING_IP_CONFIG(s_ip6));
        if (num_addresses > 0)
        {
            NMIPAddress *address = nm_setting_ip_config_get_address(NM_SETTING_IP_CONFIG(s_ip6), 0);
            std::vector<unsigned char> addressIpv6;
            ConvertIpv6Address(nm_ip_address_get_address(address), addressIpv6);
            std::cout << "IP address: ";
            for (const auto &byte : addressIpv6)
            {
                std::cout << std::setfill('0') << std::setw(2) << std::hex << (int)byte << ' ';
            }
            std::cout << std::endl;
        }
    }
}

// int main()
// {
//     WifiDeviceConfigV4 wifiDeviceConfigV4;

//     wifiDeviceConfigV4.ssid = "TEST";
//     wifiDeviceConfigV4.bssid = "A2:B2:C2:D2:E2:F2";
//     wifiDeviceConfigV4.preSharedKey = "12345678";
//     wifiDeviceConfigV4.isHiddenSsid = "false";
//     wifiDeviceConfigV4.securityType = WifiSecurityType::WIFI_SEC_TYPE_PSK;
//     wifiDeviceConfigV4.netId = -1;
//     wifiDeviceConfigV4.ipType = 1;
//     wifiDeviceConfigV4.creatorUid = 7;
//     wifiDeviceConfigV4.disableReason = 0;
//     wifiDeviceConfigV4.randomMacType = 0;
//     wifiDeviceConfigV4.randomMacAddr = "11:22:33:44:55:66";
//     wifiDeviceConfigV4.staticIp.ipAddress = 1284752956;
//     wifiDeviceConfigV4.staticIp.gateway = 1284752936;
//     NMClient *client = init_nm_client();
//     NMConnection *connection = create_wifi_connectionV4(wifiDeviceConfigV4);
//     add_connection(client, connection);

//     print_connection_info(connection);

//     g_object_unref(connection);
//     g_object_unref(client);

//     return 0;
// }