#include <iostream>
#include <NetworkManager.h>
#include <algorithm>
#include <arpa/inet.h>
#include <cstring>
#include <iomanip>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#include <glib.h>
#include <gio/gio.h>

struct WifiStatus
{
    bool wifi_enabled = false;
    NMClient *client = nullptr;
    GMainLoop *loop = nullptr;
    NMDevice *wifi_device = nullptr;
    int attempts = 0;
};

bool init_wifi_device(WifiStatus *wifi_status)
{
    GError *error = NULL;
    NMClient *client = NULL;
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        g_print("Could not connect to NetworkManager: %s\n", error->message);
        g_error_free(error);
        return false;
    }
    wifi_status->client = client;

    const GPtrArray *devices = nm_client_get_devices(client);
    if (devices == NULL)
    {
        g_print("Could not get devices: %s\n", error->message);
        g_error_free(error);
        return false;
    }

    for (guint i = 0; i < devices->len; i++)
    {
        NMDevice *device = (NMDevice *)g_ptr_array_index(devices, i);
        if (NM_IS_DEVICE_WIFI(device))
        {
            wifi_status->wifi_device = device;
            break;
        }
    }
    if (wifi_status->wifi_device == NULL)
    {
        g_print("Could not find WiFi device\n");
        return false;
    }
    return true;
}
void add_connection(NMClient *client, NMConnection *connection, GMainLoop *main_loop)
{
    g_assert(client);
    g_assert(connection);

    nm_client_add_connection_async(
        client, connection, TRUE, NULL,
        [](GObject *source_object, GAsyncResult *res, gpointer user_data)
        {
            GError *error = nullptr;
            NMRemoteConnection *remote_conn = nm_client_add_connection_finish(NM_CLIENT(source_object), res, &error);
            if (error)
            {
                g_print("Failed to add connection: %s\n", error->message);
                g_error_free(error);
                return;
            }
            // Connection added successfully
            g_print("Connection added successfully\n");
            g_object_unref(remote_conn);
            g_main_loop_quit((GMainLoop *)user_data); // stop the main loop
        },
        main_loop); // pass the main loop as user data to the callback
}

void device_state_changed_cb(NMDevice *dev, NMDeviceState state, gpointer user_data)
{
    if (state == NM_DEVICE_STATE_ACTIVATED)
    {
        g_print("Device has successfully connected to the network.\n");
        g_main_loop_quit((GMainLoop *)user_data); // stop the main loop
    }
    else if (state == NM_DEVICE_STATE_DISCONNECTED)
    {
        g_print("Device has disconnected from the network.\n");
        g_main_loop_quit((GMainLoop *)user_data); // stop the main loop
    }
}

void connect_to_network(NMClient *client, NMDevice *device, NMConnection *connection, GMainLoop *main_loop)
{
    g_assert(client);
    g_assert(device);
    g_assert(connection);

    // Connect the device state change callback to the 'state
    g_signal_connect(device, "state-changed", G_CALLBACK(device_state_changed_cb), main_loop);

    // Activate the connection
    nm_client_activate_connection_async(
        client, connection, device, NULL, NULL,
        [](GObject *source_object, GAsyncResult *res, gpointer user_data)
        {
            GError *error = NULL;
            NMActiveConnection *active_conn = nm_client_activate_connection_finish(NM_CLIENT(source_object), res, &error);
            if (error)
            {
                g_print("Failed to activate connection: %s\n", error->message);
                g_error_free(error);
                g_main_loop_quit((GMainLoop *)user_data); // stop the main loop in case of error
                return;
            }

            // Connection activated successfully
            g_print("Connection activated successfully\n");
            g_object_unref(active_conn);
        },
        main_loop);

    // Run the main loop to start monitoring events
    g_main_loop_run(main_loop);
}

void print_connection_info(NMConnection *connection)
{
    NMSettingConnection *s_con;
    NMSettingWireless *s_wifi;
    NMSettingWirelessSecurity *s_wsec;
    NMSettingIP4Config *s_ip4;
    NMSettingIP6Config *s_ip6;

    s_con = NM_SETTING_CONNECTION(nm_connection_get_setting(connection, NM_TYPE_SETTING_CONNECTION));
    s_wifi = NM_SETTING_WIRELESS(nm_connection_get_setting(connection, NM_TYPE_SETTING_WIRELESS));
    s_wsec = NM_SETTING_WIRELESS_SECURITY(nm_connection_get_setting(connection, NM_TYPE_SETTING_WIRELESS_SECURITY));
    s_ip4 = NM_SETTING_IP4_CONFIG(nm_connection_get_setting(connection, NM_TYPE_SETTING_IP4_CONFIG));
    s_ip6 = NM_SETTING_IP6_CONFIG(nm_connection_get_setting(connection, NM_TYPE_SETTING_IP6_CONFIG));

    if (s_con)
    {
        std::cout << "Connection ID: " << nm_setting_connection_get_id(s_con) << std::endl;
        std::cout << "Connection UUID: " << nm_setting_connection_get_uuid(s_con) << std::endl;
        std::cout << "Autoconnect: " << (nm_setting_connection_get_autoconnect(s_con) ? "true" : "false") << std::endl;
    }

    if (s_wifi)
    {
        GBytes *ssid = nm_setting_wireless_get_ssid(s_wifi);
        if (ssid)
        {
            gsize ssid_len = 0;
            const char *ssid_data = (const char *)g_bytes_get_data(ssid, &ssid_len);
            std::string ssid_str(ssid_data, ssid_len);
            std::cout << "SSID: " << ssid_str << std::endl;
        }
        const char *bssid = nm_setting_wireless_get_bssid(s_wifi);
        if (bssid)
        {
            std::string bssid_string = nm_utils_hwaddr_ntoa(bssid, ETH_ALEN);
            std::cout << "BSSID: " << bssid_string << std::endl;
        }
    }

    if (s_wsec)
    {
        std::cout << "Key management: " << nm_setting_wireless_security_get_key_mgmt(s_wsec) << std::endl;
        std::cout << "Pre-shared key: " << nm_setting_wireless_security_get_psk(s_wsec) << std::endl;
    }
}

int main()
{
    WifiStatus wifi_status;
    GMainLoop *main_loop = g_main_loop_new(NULL, FALSE);
    wifi_status.wifi_enabled = init_wifi_device(&wifi_status);

    const GPtrArray *connections = nm_client_get_connections(wifi_status.client);

    const char *connection_uuid = "1c50e342-02f6-4863-bbca-0449ea4a66c2"; // Replace with the UUID of your connection
    NMConnection *connection = nullptr;
    for (guint i = 0; i < connections->len; ++i)
    {
        NMConnection *conn = NM_CONNECTION(connections->pdata[i]);
        const char *conn_uuid = nm_setting_connection_get_uuid(NM_SETTING_CONNECTION(nm_connection_get_setting(conn, NM_TYPE_SETTING_CONNECTION)));
        if (g_strcmp0(conn_uuid, connection_uuid) == 0)
        {
            connection = conn;
            break;
        }
    }
    if (connection != nullptr)
    {
        // Connection with the specified UUID found
        print_connection_info(connection);
        connect_to_network(wifi_status.client, wifi_status.wifi_device, connection, main_loop);
        g_main_loop_unref(main_loop);
    }

    g_object_unref(wifi_status.client);

    return 0;
}
