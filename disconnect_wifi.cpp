#include <NetworkManager.h>
#include <glib.h>
#include <iostream>

void on_deactivate_connection_finished(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
    GError *error = NULL;
    gboolean success = nm_client_deactivate_connection_finish(NM_CLIENT(source_object), res, &error);
    if (!success)
    {
        g_print("Error deactivating connection: %s\n", error->message);
        g_error_free(error);
    }
    else
    {
        g_print("WiFi connection disconnected successfully\n");
    }
    GMainLoop *loop = (GMainLoop *)user_data;
    g_main_loop_quit(loop);
}

bool disconnect_wifi()
{
    GError *error = NULL;
    NMClient *client = NULL;
    client = nm_client_new(NULL, &error);
    if (!client)
    {
        g_print("Could not connect to NetworkManager: %s\n", error->message);
        g_error_free(error);
        return false;
    }

    const GPtrArray *devices = nm_client_get_devices(client);
    if (devices == NULL)
    {
        g_print("Could not get devices: %s\n", error->message);
        g_error_free(error);
        return false;
    }
    NMDevice *wifi_device = NULL;
    NMActiveConnection *active_connection = NULL;
    for (guint i = 0; i < devices->len; i++)
    {
        NMDevice *device = (NMDevice *)g_ptr_array_index(devices, i);
        if (NM_IS_DEVICE_WIFI(device))
        {
            wifi_device = device;
            active_connection = nm_device_get_active_connection(wifi_device);
            break;
        }
    }

    if (active_connection == NULL)
    {
        g_print("No active WiFi connection found\n");
        return false;
    }
    GMainLoop *loop = g_main_loop_new(NULL, FALSE);
    nm_client_deactivate_connection_async(client, active_connection, NULL, on_deactivate_connection_finished, loop);
    g_main_loop_run(loop);
    g_main_loop_unref(loop);

    return true;
}

int main(int argc, char *argv[])
{
    bool result = disconnect_wifi();
    if (result)
    {
        g_print("WiFi connection disconnected!\n");
    }
    else
    {
        g_print("Could not disconnect WiFi connection.\n");
    }
    return 0;
}
